const path = require('path')
const glob = require('glob')
const webpack = require('webpack')
const manifest = require('../manifest.json')
const CleanWebpackPlugin = require('clean-webpack-plugin')

let files = glob.sync('./resources/modules/*/index.js')
let entrys = {}
for (let file of files) {
    let tmp = file.split('/')
    tmp = tmp.splice(-2).join('/')
    entrys[tmp.substr(0, tmp.length - 3)] = file
}

module.exports = {
    mode: 'production',
    entry: entrys,
    output: {
        path: path.resolve(__dirname, '../public/build'),
        publicPath: '/build',
        filename: '[name].js'
    },
    resolve: {
        extensions: ['.js', '.vue', '.json']
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: ['babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.(mp4|webm|ogg|mp3|wav|flac|aac)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: 'media/[name].[hash:7].[ext]'
                }
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 10000,  //8k一下的转义为base64
                        name: 'images/[name].[hash:7].[ext]'
                    }
                }]
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: 'url-loader',
                options: {
                    limit: 10000,
                    name: 'fonts/[name].[hash:7].[ext]'
                }
            }
        ]
    },
    plugins: [
        new webpack.DllReferencePlugin({
            ...manifest
        }),
        new CleanWebpackPlugin([path.resolve(__dirname, '../public/build/')]),
    ]
}
