<?php
namespace app\account\controller\index;

use app\common\builder\core\Form;
use app\common\builder\types\FormField;
use app\common\controller\Base;
use think\captcha\Captcha;

class Login extends Base
{

    /**
     * @route('/login', 'get')
     */
    public function index()
    {
        $form = new Form();
        $form->setTitle('登陆控制面板')
            ->setRequest('post')
            ->setSubmitText('马上登陆')
            ->setStyle([
                'max-width'=>'500px'
            ])
            ->addField(FormField::Input, 'username', '登陆账号', [
                'required'=>false
            ])
            ->addField(FormField::Password, 'password', '登陆密码')
            ->addCaptcha(url('account/index.login/verify'))
        ;

        return $form->render();
    }


    /**
     * 验证码
     * @route('/login/verify', 'get')
     */
    public function verify()
    {
        $captcha = new Captcha();
        return $captcha->entry();
    }


}
