<?php
// +----------------------------------------------------------------------
// | build-admin.
// +----------------------------------------------------------------------
// | FileName: Ke.php
// +----------------------------------------------------------------------
// | Author: King east <1207877378@qq.com>
// +----------------------------------------------------------------------


namespace app\common\taglib;


use think\Exception;
use think\facade\App;
use think\template\TagLib;

class Ke extends TagLib
{
    // 标签定义
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'webpack'     => ['attr' => 'file', 'close' => 0],
    ];

    protected function tagWebpack($attr, $content)
    {
        $version = '';
        if (App::isDebug()) {
            $version = '?v=' . $_SERVER['REQUEST_TIME'];
        }
        return '<script src="/build/' . $attr['file'] . '.js' . $version . '"></script>';



    }


}