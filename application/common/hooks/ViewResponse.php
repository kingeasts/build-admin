<?php
// +----------------------------------------------------------------------
// | build-admin.
// +----------------------------------------------------------------------
// | FileName: ViewResponse.php
// +----------------------------------------------------------------------
// | Author: King east <1207877378@qq.com>
// +----------------------------------------------------------------------


namespace app\common\hooks;


use think\facade\App;
use think\facade\Config;
use think\Request;

class ViewResponse
{
    public function run(Request $request)
    {
//        $module = $request->module();
//        $action = $request->action();
//
//        $ret = $this->getSceneName($request->controller());
//        if (is_array($ret)) {
//            $scene = $ret[0];
//            $controller = implode('/', $ret[1]);
//        } else {
//            $scene = '';
//            $controller = $ret;
//        }




        return \think\facade\View::fetch();
//        return \think\facade\View::fetch(App::getAppPath() . $module . '/view/' . $scene . '/' . $controller . '/' . $action . '.' . Config::get('template.view_suffix'));
//        var_dump($scene);
//        var_dump($module);
//        var_dump($controller);
//        var_dump($action);
//        exit;
    }


    /**
     * 获取场景名称
     * @param string $str
     * @return string|array
     */
    private function getSceneName($str)
    {
        if (strpos($str, '.') === false) {
            return strtolower($str);
        } else {
            $stmp = explode('.', $str);
            $str = array_shift($stmp);

            return [strtolower($str), $stmp];
        }
    }

}