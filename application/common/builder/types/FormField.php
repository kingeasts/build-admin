<?php
// +----------------------------------------------------------------------
// | build-admin.
// +----------------------------------------------------------------------
// | FileName: FormField.php
// +----------------------------------------------------------------------
// | Author: King east <1207877378@qq.com>
// +----------------------------------------------------------------------


namespace app\common\builder\types;


class FormField
{
    const Input = 'input';

    const Textarea = 'textarea';

    const Password = 'password';

    const Select = 'select';

    const Radio = 'radio';

    const Checkbox = 'checkbox';

    const Switch = 'switch';
}
