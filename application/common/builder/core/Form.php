<?php
// +----------------------------------------------------------------------
// | build-admin.
// +----------------------------------------------------------------------
// | FileName: Form.php
// +----------------------------------------------------------------------
// | Author: King east <1207877378@qq.com>
// +----------------------------------------------------------------------


namespace app\common\builder\core;


use app\common\builder\Base;

class Form extends Base
{
    public function init()
    {
        $this->name = 'form';
        $this->vars['method'] = 'post';
        $this->vars['submit_text'] = '立即提交';
        $this->vars['show_reset'] = true;
        $this->vars['form'] = [];
    }

    /**
     * 是否显示重置按钮
     * @param bool $v
     * @return $this
     */
    public function showReset($v)
    {
        $this->vars['show_reset'] = $v;
        return $this;
    }

    /**
     * 设置请求信息
     * @param string $method
     * @param string $url
     * @return $this
     */
    public function setRequest($method, $url = '')
    {
        $this->vars['method'] = $method;
        $this->vars['url'] = $url;
        return $this;
    }


    /**
     * 设置表单数据
     * @param array $data
     * @return $this
     */
    public function setData(array $data)
    {
        $this->vars['form'] = $data;
        return $this;
    }

    /**
     * 设置提交按钮文本
     * @param string $str
     * @return $this
     */
    public function setSubmitText($str)
    {
        $this->vars['submit_text'] = $str;
        return $this;
    }


    /**
     * 添加控件
     * @param string $type
     * @param string $field
     * @param string $label
     * @param array $options
     * @return $this
     */
    public function addField($type, $field, $label = '', array $options = [])
    {
        $this->pushVars('controls', [
            'type'=>$type,
            'name'=>$field,
            'label'=>$label,
            'options'=>$options
        ]);
        return $this;
    }


    public function addCaptcha($img, $name = 'checkcode', $label = '验证码')
    {
        $this->pushVars('controls', [
            'type'=>'checkimg',
            'name'=>$name,
            'src'=>$img,
            'label'=>$label
        ]);
        return $this;
    }

}